# ros amcl解读初步

Summarized by PhilHE on July/August 2018

## I. 概览

### 1.1 amcl包介绍

#### 1.1.1 从Navigation Metapackage说起

> ROS wiki中的navigation界面在[此处](http://wiki.ros.org/navigation/)，源码部分在[ros-planning/navigation](https://github.com/ros-planning/navigation)处下载

<img src="https://i.loli.net/2018/08/23/5b7e3c2b63d79.jpg" alt="navi.jpg" title="navi.jpg" width=800/>

就总体而言Navigation Stack是一个ROS的metapackage，里面包含了ROS在路径规划、定位、地图、异常行为恢复等方面的package，其中运行的算法都堪称经典。Navigation Stack的主要作用就是路径规划，通常是输入各传感器的数据，输出速度。

在这大架构中，amcl与SLAM的框架很像，最主要的区别是`/map`作为了输入，而不是输出，因为AMCL算法只负责定位，而不管建图。 

同时还有一点需要注意，AMCl定位会对里程计误差进行修正，修正的方法是把里程计误差加到`map_frame`和`odom_frame`之间，而`odom_frame`和`base_frame`之间是里程计的测量值，这个测量值并不会被修正。这一工程实现与gmapping、karto的做法是相同的。

#### 1.1.2 amcl包概览

源码部分在[ros-planning/navigation/amcl(kinetic-devel)](https://github.com/ros-planning/navigation/tree/kinetic-devel/amcl)处可以下载（注意调整至`kinetic-devel`处），以下部分是对[ROS wiki - amcl](http://wiki.ros.org/amcl)部分的一个阐发：

> amcl is a probabilistic localization system for a robot moving in 2D. It implements the adaptive (or KLD-sampling) Monte Carlo localization approach (as described by Dieter Fox), which uses a particle filter to track the pose of a robot against a known map.

amcl 是一种二维移动机器人的概率定位系统， 它实现了**自适应(或 KLD-sampling)蒙特卡罗定位方法**，该方法使用一个粒子滤波器来跟踪一个机器人对着已知地图的姿势。

在wiki后续的介绍中可以知道，amcl使用激光地图，激光扫描，坐标变换信息，输出位姿估计。开始运行阶段，amcl根据所提供的参数，初始化它的粒子滤波器。 

简而言之，amcl作为一个2D的概率定位系统，输入激光雷达数据、里程计数据，输出机器人在地图中的位姿，核心为自适应蒙特卡洛定位方法，通过在已知地图中使用粒子滤波方法得到位姿。

### 1.2 要点TF变换

本文使用到的参考系仅限于全局参考系（world frame），机器人中心参考系（base frame）以及里程计参考系（odometry frame）。

`amcl`将传入的激光扫描转换为里程计odom_frame。 因此必须存在，一条从激光扫描帧发布到里程计帧的tf树路径。

一个实现细节：在收到第一个激光扫描后，`amcl`查找激光帧和基础帧base_frame之间的变换，并保持不变。 所以amcl无法处理相对于基座移动的激光发射器。

下图显示了使用odometry和amcl定位之间的差异。 在运行期间，amcl先初步估计base_frame相对于全局global_frame的变换，但是最后它仅发布全局到里程计odom_frame之间的变换，然后通过测量模型得到base_frame相对于map_frame（全局地图坐标系），也就知道了机器人在地图中的位姿。

从本质上讲，这种变换解释了使用**航位推算**过程中发生的漂移以及时候amcl后带来的修正。

**航位推测法**，Dead reckoning是一种利用现在物体位置及速度推定未来位置方向的航海技术，现已应用至许多交通技术层面，但容易受到误差累积的影响。

![amcl_localization.png](./pic/amcl_localization.png)

### 1.3 节点分析——话题、服务与参数

#### 1.3.1 **Subscribed Topics | 订阅话题**

| 话题与消息                                            | 作用                                                         |
| ----------------------------------------------------- | ------------------------------------------------------------ |
| scan (sensor_msgs/LaserScan)                          | 获取激光扫描信息                                             |
| tf (tf/tfMessage)                                     | 获取坐标变换信息                                             |
| initialpose (geometry_msgs/PoseWithCovarianceStamped) | 用于(重新)初始化粒子滤波器的**均值和协方差**（**Mean and covariance**） |
| map (nav_msgs/OccupancyGrid)                          | 设置`use_map_topic`参数后，amcl 订阅此主题以检索用于激光定位的地图。（在导航包1.4.2中的新特性） |

对于最后一个。当`use_map_topic `参数已被设置，amcl订阅此主题，以检索用于激光定位的地图

#### 1.3.2 **Published Topics | 发布话题**

| 话题与消息                                          | 作用                                   |
| --------------------------------------------------- | -------------------------------------- |
| amcl_pose (geometry_msgs/PoseWithCovarianceStamped) | 发布在地图中带有协方差的估计位姿       |
| particlecloud (geometry_msgs/PoseArray)             | 发布点云数据，该组位姿由滤波器估计得出 |
| tf (tf/tfMessage)                                   | 发布从位置里程计到地图中间的位姿变换   |

#### 1.3.3 **Services | 服务**

| 服务                                     | 作用                                                     |
| ---------------------------------------- | -------------------------------------------------------- |
| global_localization (std_srvs/Empty)     | 初始化全局坐标。其中所有粒子在地图中的自由空间中随机分布 |
| request_nomotion_update (std_srvs/Empty) | 用于手动更新与发布更新点（云）的服务                     |

#### 1.3.4 **Services Called | 调用服务**

`static_map 静态地图 (nav_msgs/GetMap 导航 / GetMap)`

amcl 调用这个服务来检索用于激光定位的地图; 启动模块从服务中获得地图

#### 1.3.5 **Parameters | 参数**

有三类ROS参数可用于配置 amcl 节点: <u>整体滤波器参数、激光模型参数和里程计模型参数。</u>

本部分具体解读参见**《amcl_lib解读》**

**1）整体滤波器参数**

> 对于点云、位姿、地图、数据发布等进行配置。该部分参数涉及较多方面比较繁杂

- 关键参数

```
~min_particles (int, default: 100)
允许的最少粒子数
~max_particles (int, default: 5000)
允许的最多粒子数

~kld_err (double, default: 0.01)
真实分布与估计分布之间最大误差
~kld_z (double, default: 0.99)

~update_min_d (double, default: 0.2 meters)
执行一次滤波器更新所需的平移距离
~update_min_a (double, default: π/6.0 radians)
执行一次滤波器更新所需的旋转角度

~resample_interval (int, default: 2)
重采样之前滤波器更新次数

~gui_publish_rate (double, default: -1.0 Hz)
指定最大可用多大速率(Hz)扫描并发布用于可视化的路径， -1.0 to disable
~save_pose_rate (double, default: 0.5 Hz)
指定存储上次估计的位姿和协方差到参数服务器的最大速率 (Hz)，存储变量为 ~initial_pose_* and ~initial_cov_*。保存的位姿会在后面初始化滤波器时候使用。 -1.0 to disable

~use_map_topic (bool, default: false)
如果设置为true, AMCL将订阅地图主题，不会使用service call获取地图。New in navigation 1.4.2
~first_map_only (bool, default: false)
如果设置为true，AMCL将使用订阅到的第一个地图，不会使用每次更新获取的新地图。New in navigation 1.4.2
```

**2）激光模型变量参数**

- 介绍

无论使用什么混合权重，权重加总应该等于1。 `beam model`使用了所有的4种权重: `z_hit`, `z_short`, `z_max`, and `z_rand`。`likelihood_field model`仅仅使用了2种: `z_hit` and `z_rand`。

- 关键参数

```
~laser_min_range (double, default: -1.0)
最小扫描范围; -1.0 will cause the laser's reported minimum range to be used.
~laser_max_range (double, default: -1.0)
最大扫描范围; -1.0 will cause the laser's reported maximum range to be used.
~laser_max_beams (int, default: 30)
当更新滤波器时候，每次扫描有多少均匀分布（等间隔的）beam被使用
~laser_z_hit (double, default: 0.95)
模型z_hit部分混合权重
~laser_z_short (double, default: 0.1)
模型z_short部分混合权重
~laser_z_max (double, default: 0.05)
模型z_max部分混合权重
~laser_z_rand (double, default: 0.05)
模型z_rand部分混合权重
~laser_sigma_hit (double, default: 0.2 meters)
模型z_hit部分使用的高斯模型标准差混合权重
~laser_lambda_short (double, default: 0.1)
模型z_short部分指数衰减参数
~laser_likelihood_max_dist (double, default: 2.0 meters)
Maximum distance to do obstacle inflation on map, for use in likelihood_field model.

~laser_model_type (string, default:"likelihood_field")
模型类型（beam, likelihood_field或者likelihood_field_prob） (same aslikelihood_field but incorporates the beamskip feature, if enabled).
```

**3）里程计模型参数**

- 介绍

如果参数`~odom_model_type`是`diff`，那么使用`sample_motion_model_odometry`算法，这种模型使用噪声参数``odom_alpha_1``到``odom_alpha4`。

` 如果参数``~odom_model_type` 是`"omni"`，那么使用客制化模型用于全向底座，使用噪声参数`odom_alpha_1`到`odom_alpha_5`。前4个参数类似于`diff`模型，第5个参数用于捕获机器人在垂直于前进方向的位移（没有旋转）趋势。

由于旧有的模型有bug，解决了bug的新模型使用了新的类型`"diff-corrected"` `和``"omni-corrected"`。参数`odom_alpha`缺省值仅仅适合旧的模型，对于新的模型这些值要小很多，请参考[ROS Amswer: Tuning AMCL's diff-corrected and omni-corrected odom models](http://answers.ros.org/question/227811/tuning-amcls-diff-corrected-and-omni-corrected-odom-models/)。

- 关键参数

```
~odom_model_type (string, default: "diff")
模型类型（"diff", "omni", "diff-corrected" or"omni-corrected"）
diff：2轮差分    omni：全向轮，y方向有速度，后两者为修正模型

~odom_alpha1 (double, default: 0.2)
指定里程旋转估计（机器人运动旋转分量）中期望的噪声
~odom_alpha2 (double, default: 0.2)
指定里程旋转估计（机器人运动位移分量）中期望的噪声
~odom_alpha3 (double, default: 0.2)
指定里程位移估计（来自机器人运动位移分量）中期望的噪声
~odom_alpha4 (double, default: 0.2)
指定里程位移估计（来自机器人运动旋转分量）期望噪声
~odom_alpha5 (double, default: 0.2)
位移相关噪声参数 (only used if model is"omni").

~odom_frame_id (string, default:"odom")
odometry使用的坐标系
~base_frame_id (string, default:"base_link")
基座坐标系
~global_frame_id (string, default:"map")
全局坐标系
~tf_broadcast (bool, default: true)
设置为false，amcl将不会发布全局坐标系和里程坐标系之间的转换
```

## II. 相关算法讨论

amcl的英文全称是adaptive Monte Carlo localization，其实就是蒙特卡洛定位方法的一种升级版，使用自适应的KLD方法来更新粒子，，有兴趣的可以去看：[KLD](https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence)。 

### 2.1 相关论文与资料

#### 2.1.1 论文

- Fox D. [KLD-sampling: Adaptive particle filters](http://papers.nips.cc/paper/1998-kld-sampling-adaptive-particle-filters.pdf)[C]//Advances in neural information processing systems. 2002: 713-720.
- Fox D, Burgard W, Dellaert F, et al. [Monte carlo localization: Efficient position estimation for mobile robots](http://www.aaai.org/Papers/AAAI/1999/AAAI99-050.pdf)[J]. AAAI/IAAI, 1999, 1999(343-349): 2.2.

#### 2.1.2 书籍

- Thrun S, Burgard W, Fox D. [Probabilistic robotics](https://books.google.com.hk/books?hl=zh-CN&lr=&id=wjM3AgAAQBAJ&oi=fnd&pg=PR7&dq=Probabilistic+robotics&ots=22i0slpnmJ&sig=ISbXUfHMTPbe-aNT9SFOVIuBXsg&redir_esc=y&hl=zh-CN&sourceid=cndr)[M]. MIT press, 2005.
- Fox D, Thrun S, Burgard W, et al. [Particle filters for mobile robot localization](https://link.springer.com/chapter/10.1007/978-1-4757-3437-9_19)[M]//Sequential Monte Carlo methods in practice. Springer, New York, NY, 2001: 401-428.

#### 2.1.3 链接

- [Monte Carlo localization - Wikipedia ](https://en.wikipedia.org/wiki/Monte_Carlo_localization)

- [Adaptive Monte Carlo Localization - Robotics Knowledgebase ](http://roboticsknowledgebase.com/wiki/state-estimation/adaptive-monte-carlo-localization/)

- [PROBABILISTIC-ROBOTICS.ORG ](http://www.probabilistic-robotics.org/)

### 2.2 算法基本

#### 2.2.1 关键概念

- robot location and navigation 机器人定位与导航

[Robot location - Wikipedia ](https://en.wikipedia.org/wiki/Robot_navigation)

- particle filter 粒子滤波

[Particle filter - Wikipedia ](https://en.wikipedia.org/wiki/Particle_filter)

也成为顺序蒙特卡洛算法（Sequential Monte Carlo, SMC）

- Monte Carlo Location 蒙特卡洛定位

[Monte Carlo localization - Wikipedia ](https://en.wikipedia.org/wiki/Monte_Carlo_localization)

#### 2.2.2 如何实现自适应？ | How to realize 'adaptive' ?

`to be contimued`

### 2.3 主要算法分析

概率机器人一书中讲述了许多算法以及它们使用的参数，建议读者去书中查看以获得更多细节信息。 这里我们用到书中的算法包括：

1. sample_motion_model_odometry
2. beam_range_finder_model
3. likelihood_field_range_finder_model
4. Augmented_MCL
5. KLD_Sampling_MCL

## III. 代码分析

### 3.1 `package.xml`

依赖部分是整个软件包清单的重点，主要呈现在下方：

在[REP 140 - Package Manifest Format Two Specification](http://www.ros.org/reps/rep-0140.html)中，目前软件包建议将较旧的格式1包迁移为格式2；但由于amcl溯源已久，因而其`<package>`并不是现在常见的`<package format="2">`（最新的melodic-devel已经更新)。

```xml
<buildtool_depend>catkin</buildtool_depend>
    <build_depend>rosbag</build_depend>
    <build_depend>dynamic_reconfigure</build_depend>
    <build_depend>message_filters</build_depend>
    <build_depend>nav_msgs</build_depend>
    <build_depend>roscpp</build_depend>
    <build_depend>std_srvs</build_depend>
    <build_depend>tf</build_depend>

    <run_depend>rosbag</run_depend>
    <run_depend>roscpp</run_depend>
    <run_depend>dynamic_reconfigure</run_depend>
    <run_depend>tf</run_depend>
    <run_depend>nav_msgs</run_depend>
    <run_depend>std_srvs</run_depend>

    <test_depend>rostest</test_depend>
    <test_depend>map_server</test_depend>
```

- buildtool_depend：首先是一般化的catkin编译工具
- build_depend：在生成时需要的依赖包方面，rosbag主要用于相关包文件的处理；则可以在`/cfg`文件夹有所体现实现了节点参数的动态配置；`message_filters`主要运用在相关信息的过滤，`nav_msgs`则是整个导航源包共同的依赖，`std_srvs`是服务类型使用的体现，`tf`则是整个amcl的重中之重，实现从`base`到`map`的一个转换。
- run_depend：运行时所需要的依赖包，一般同上（在<package format="2">`一般写作<exec_depend>`）。
- test_depend：用于测试的依赖包。运用了ros常用的测试工具`rostest`以及提供 map 数据的`map_server`

### 3.2 `CMakeLists.txt`

相比于一般应用型的节点，amcl的`CMakeLists.txt`较为丰富，有助于我们对`amcl`的理解与运用：

- **dynamic reconfigure** 参数动态配置文件的增加

```cmake
# dynamic reconfigure
generate_dynamic_reconfigure_options(
    cfg/AMCL.cfg
)
```

- add_library 生成三个库文件**amcl_pf　amcl_map　amcl_sensors**

```cmake
add_library(amcl_pf
                    src/amcl/pf/pf.c
                    src/amcl/pf/pf_kdtree.c
                    src/amcl/pf/pf_pdf.c
                    src/amcl/pf/pf_vector.c
                    src/amcl/pf/eig3.c
                    src/amcl/pf/pf_draw.c)
...
target_link_libraries(amcl_sensors amcl_map amcl_pf)
```

- Configure Tests：比较与众不同的而是增加了测试部分，可以使用其中所提供的方式对amcl进行一个测试

```cmake
## Configure Tests
if(CATKIN_ENABLE_TESTING)
  find_package(rostest REQUIRED)
  
# Bags
...
# Maps
...
# Tests
...
```

### 3.3 `/src`

#### 3.3.1 `amcl_node.cpp`

文件实现了上述的amcl节点功能

主要有将近二十个函数，详见**《amcl_node解读》**

#### 3.3.2 `amcl` libraries

文件夹内生成了amcl节点的相关库函数，主要是map地图,pf,粒子滤波器以及sensos三个方面

主要有将近十个函数，详见**《amcl_lib解读》**

### 3.4 `/include`

![head_for_amcl_libraries](./pic/head_for_amcl_libraries.png)

由上图可知，`/include`文件夹主要为`amcl` 所生成三个库函数**amcl_pf　amcl_map　amcl_sensors**的头文件。

### 3.5 `/cfg`

#### 3.5.1 概要

Dynamic Reconfig是ROS比较厉害的一个地方，可以在程序运行的时候动态调整参数

本文件夹主要用于告诉ROS 到时侯我们需要动态调整的参数

#### 3.5.2 amcl实现

- AMCL.cfg

```python
#!/usr/bin/env python

PACKAGE = 'amcl'

from math import pi
from dynamic_reconfigure.parameter_generator_catkin import ParameterGenerator, int_t, double_t, str_t, bool_t

gen = ParameterGenerator()

# 粒子等相关参数配置
# Particles numbers Parameters
gen.add("min_particles", int_t, 0, "Minimum allowed number of particles.", 100, 0, 1000)
gen.add("max_particles", int_t, 0, "Mamimum allowed number of particles.", 5000, 0, 10000)

...

# 地图参数配置
# Map Parameters
gen.add("use_map_topic", bool_t, 0, "When set to true, AMCL will subscribe to the map topic rather than making a service call to receive its map.", False)
gen.add("first_map_only", bool_t, 0, "When set to true, AMCL will only use the first map it subscribes to, rather than updating each time a new one is received.", False)

# 激光模型参数配置
# Laser Model Parameters
...

# 里程计模型参数配置
# Odometry Model Parameters
...

gen.add("odom_frame_id", str_t, 0, "Which frame to use for odometry.", "odom")
gen.add("base_frame_id", str_t, 0, "Which frame to use for the robot base.", "base_link")
gen.add("global_frame_id", str_t, 0, "The name of the coordinate frame published by the localization system.", "map")

gen.add("restore_defaults", bool_t, 0, "Retsore the default configuration", False)

# 生成AMCLConfig.h头文件
exit(gen.generate(PACKAGE, "amcl_node", "AMCL"))
```

这里尤其要注意最后一句的第三个参数，必须与config文件同名，我们的config文件名为AMCL.cfg所以这里为AMCL。而且第三个参数也决定着我们的config头文件的名称，后面埋个伏笔，头文件生成的名称为AMCLConfig.h也即，第三个参数加上Config.h.

- AMCLConfig.h

在`src/amcl_node.cpp`头文件我们可以观察到即为Dynamic Reconfig的实现

```c++
// Dynamic_reconfigure 动态参数配置
#include "dynamic_reconfigure/server.h"
#include "amcl/AMCLConfig.h"
```

对应的在前文提到

在package.xml中的依赖项即有`<build_depend>dynamic_reconfigure</build_depend>`

在`CMakeLists.txt`有`generate_dynamic_reconfigure_options(xcfg/AMCL.cfg)`

#### 3.5.3 参考

Dynamic Reconfig的讲解主要有三处

1. <http://wiki.ros.org/dynamic_reconfigure/Tutorials>

2. <http://wiki.ros.org/roscpp_tutorials/Tutorials> 
3. <http://wiki.ros.org/ROSNodeTutorialC++> 

## 参考资料

Related stacks:

- <http://github.com/ros-planning/navigation_msgs> (new in Jade+)
- <http://github.com/ros-planning/navigation_tutorials>
- <http://github.com/ros-planning/navigation_experimental>

